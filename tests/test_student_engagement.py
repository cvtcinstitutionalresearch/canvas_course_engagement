import datetime
import pytest
from canvasapi.exceptions import ResourceDoesNotExist
from canvas_course_engagement.student_engagement import (
    StudentRating,
    rate,
    get_current_student_scores,
    get_student_access_for_course,
    get_ratings_for_course,
    try_to_parse_rfc3339,
    get_first_and_last_view_for_student,
)
import mock_canvas


class TestStudentEngagementRate:
    """Tests for using the 'rate' function"""

    @pytest.mark.parametrize(
        "grade,average,percentile",
        [(85, 100, 0), (85, 0, 0), (100, 100, 100), (97, 0, 0)],
    )
    def test_rate_85(self, grade, average, percentile):
        # All students with over 85% should always be Encourages.
        assert rate(grade, average, percentile) == StudentRating.ENCOURAGE

    @pytest.mark.parametrize(
        "grade,average,percentile",
        [
            (75, 76, 99),  # Pass on course average score
            (84.9, 100, 99),  # Pass on usage percentile
            (84.9, 100, 25.1),  # Barely pass on usage percentile
            (84.9, 99.9, 99),  # Just barely pass on average score
        ],
    )
    def test_rate_75_good(self, grade, average, percentile):
        """Tests a student with 75-85% grade who passes to Encourage"""
        assert rate(grade, average, percentile) == StudentRating.ENCOURAGE

    @pytest.mark.parametrize(
        "grade,average,percentile",
        [(75, 99, 24), (75, 99, 0), (75, 90.1, 0), (84.9, 100, 0)],
    )
    def test_rate_75_bad(self, grade, average, percentile):
        """Tests a student with 75-85% grade who fails to Explore"""
        assert rate(grade, average, percentile) == StudentRating.EXPLORE

    @pytest.mark.parametrize(
        "grade,average,percentile",
        [
            (74.9, 89.9, 0),  # Pass on average alone
            (65, 80, 0),  # Pass on average alone
            (65, 81, 100),  # Pass on usage percentile
            (65, 81, 25.1),  # Barely pass on usage percentile
            (72.74, 67.55, 69.23),
            (72.11, 86.5, 42.11),
            (73.02, 67.64, 68.18),
            (73.49, 67.64, 36.36),
            (68, 67.64, 42.86),
            (66.61, 79.68, 76.92),  # Real-world previously failing cases
        ],
    )
    def test_rate_65_good(self, grade, average, percentile):
        """Tests a student with 65-74% grade who passes to Explore"""
        assert rate(grade, average, percentile) == StudentRating.EXPLORE

    @pytest.mark.parametrize(
        "grade,average,percentile", [(65, 81, 24), (65, 81, 0), (74, 91.1, 24)]
    )
    def test_rate_65_bad(self, grade, average, percentile):
        """Tests a student with 65-74% grade who fails to Engage"""
        assert rate(grade, average, percentile) == StudentRating.ENGAGE

    @pytest.mark.parametrize(
        "grade,average,percentile", [(64, 74, 100), (55, 65, 0), (55, 65, 100)]
    )
    def test_rate_55_good(self, grade, average, percentile):
        """Tests a student with 55-64% grade who passes to Explore"""
        assert rate(grade, average, percentile) == StudentRating.EXPLORE

    @pytest.mark.parametrize(
        "grade,average,percentile", [(64, 75, 100), (55, 66, 0), (55, 65.1, 100)]
    )
    def test_rate_55_bad(self, grade, average, percentile):
        """Tests a student with 55-64% grade who fails to Engage"""
        assert rate(grade, average, percentile) == StudentRating.ENGAGE

    @pytest.mark.parametrize(
        "grade,average,percentile",
        [(0, 10, 100), (0, 70, 100), (54, 55, 100), (54, 55, 0)],
    )
    def test_rate_0(self, grade, average, percentile):
        """Tests a student with 0-54% grade, which always fails to Engage"""
        assert rate(grade, average, percentile) == StudentRating.ENGAGE

    @pytest.mark.parametrize(
        "grade, average, percentile", [(None, 89, None), (None, 50, 100)]
    )
    def test_rate_unrated(self, grade, average, percentile):
        """Tests that a student with no grades will be unrated"""
        assert rate(grade, average, percentile) == StudentRating.UNRATED


class TestStudentEnagement:
    """Tests for the rest of the student_engagement module

    Data in these tests is based off of the mock_canvas module, edits there
    may need to be copied here.
    """

    def test_get_current_student_scores(self):
        course = mock_canvas.MockCanvas.get_course(1)
        assert get_current_student_scores(course) == {
            1: {"score": None, "user": {"id": 1, "sortable_name": "user_unrated"}},
            2: {"score": 61.33, "user": {"id": 2, "sortable_name": "user_engage"}},
            3: {"score": 71.67, "user": {"id": 3, "sortable_name": "user_explore"}},
            4: {"score": 100, "user": {"id": 4, "sortable_name": "user_encourage"}},
        }

    def test_get_current_student_scores_not_course(self):
        with pytest.raises(TypeError):
            get_current_student_scores("I am not a course (1)")

    def test_get_current_student_scores_404(self):
        course = mock_canvas.MockCanvas.get_course(404)
        assert get_student_access_for_course(course) == {}

    def test_get_student_access_for_course(self):
        course = mock_canvas.MockCanvas.get_course(1)
        assert get_student_access_for_course(course) == {1: 0, 2: 0, 3: 1, 4: 3}

    def test_get_student_access_for_course_not_course(self):
        with pytest.raises(TypeError):
            get_student_access_for_course("I am not a course (2)")

    def test_get_ratings_for_course(self):
        course = mock_canvas.MockCanvas.get_course(1)
        ratings = get_ratings_for_course(course)
        assert ratings.average_grade == pytest.approx(77.7, rel=0.01)

        # students is a dict, not a list as these indexes would seem
        assert ratings.students[1] == {
            "grade": None,
            "page_view_percentile": 37.5,
            "page_views": 0,
            "engagement": StudentRating.UNRATED,
            "user": {"id": 1, "sortable_name": "user_unrated"},
        }
        assert ratings.students[2] == {
            "grade": 61.33,
            "page_view_percentile": 37.5,
            "page_views": 0,
            "engagement": StudentRating.ENGAGE,
            "user": {"id": 2, "sortable_name": "user_engage"},
        }
        assert ratings.students[3] == {
            "grade": 71.67,
            "page_view_percentile": 75,
            "page_views": 1,
            "engagement": StudentRating.EXPLORE,
            "user": {"id": 3, "sortable_name": "user_explore"},
        }
        assert ratings.students[4] == {
            "grade": 100,
            "page_view_percentile": 100,
            "page_views": 3,
            "engagement": StudentRating.ENCOURAGE,
            "user": {"id": 4, "sortable_name": "user_encourage"},
        }

    def test_get_ratings_for_course_no_page_views(self):
        """Tests a course with a student who has no page views"""
        course = mock_canvas.MockCanvas.get_course(2)
        ratings = get_ratings_for_course(course)
        assert ratings.average_grade == pytest.approx(80.75, rel=0.01)

        assert ratings.students[4] == {
            "grade": 100,
            "page_view_percentile": 100,
            "page_views": 3,
            "engagement": StudentRating.ENCOURAGE,
            "user": {"id": 4, "sortable_name": "user_encourage"},
        }

        assert ratings.students[100] == {
            "grade": 90,
            "page_view_percentile": 0,
            "page_views": None,
            "engagement": StudentRating.ENCOURAGE,
            "user": {"id": 100, "sortable_name": "user_no_views"},
        }

    def test_get_ratings_for_course_empty(self):
        """Tests that an empty course will cause an empty return"""
        course = mock_canvas.MockCanvas.get_course(0)
        graded = get_ratings_for_course(course)
        assert graded.average_grade == None
        assert graded.students == {}

    def test_try_to_parse_rfc3339(self):
        test = datetime.datetime(2018, 1, 1, 4, 30, 1, 0, tzinfo=datetime.timezone.utc)
        rfc3339 = "2018-01-01T04:30:01+00:00"
        pythonish = "2018-01-01T04:30:01+0000"
        rfc3339_parsed = try_to_parse_rfc3339(rfc3339)
        pythonish_parsed = try_to_parse_rfc3339(pythonish)
        assert rfc3339_parsed == test
        assert rfc3339_parsed == pythonish_parsed

    def test_get_first_and_last_view_for_student(self):
        min_date = datetime.datetime(2019, 1, 2, 10, 10, 1, 0, datetime.timezone.utc)
        max_date = datetime.datetime(2019, 1, 3, 10, 10, 1, 0, datetime.timezone.utc)
        user1 = mock_canvas.MockUser(1, 100, "1", 1, min_date, max_date)
        user2 = mock_canvas.MockUser(2, 100, "2", 2, min_date, max_date)
        course = mock_canvas.MockCourse([user1, user2])

        with_number = get_first_and_last_view_for_student(course, 1)
        with_user = get_first_and_last_view_for_student(course, user1)
        assert with_number == with_user
        assert with_number == (min_date, max_date)
