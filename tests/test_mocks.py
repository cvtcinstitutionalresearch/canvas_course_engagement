"""Tests to test the mocks used for testing"""

import pytest
import datetime
from canvasapi.exceptions import ResourceDoesNotExist
from mock_canvas import MockCanvas, MockEnrollment, MockUser, MockCourse

user_id = 1
user_grade = 100
user_name = "User, Mock Enrollment"
user_page_views = 8
user = MockUser(
    user_id,
    user_grade,
    user_name,
    user_page_views,
    datetime.datetime.min,
    datetime.datetime.max,
)


class TestMockEnrollment:

    enrollment = MockEnrollment(user)

    def test_init_grades(self):
        assert self.enrollment.grades["current_score"] == 100

    def test_init_user(self):
        assert self.enrollment._user is user

    def test_init_user_dict(self):
        assert self.enrollment.user == {"id": user_id, "sortable_name": user_name}

    def teset_init_user_id(self):
        assert self.enrollment.user_id == user_id

    def test_to_dict(self):
        assert self.enrollment.to_dict() == {
            "grades": {"current_score": 100},
            "user": {"id": user_id, "sortable_name": user_name},
        }


class TestMockuser:
    def test_init_id(self):
        assert user.id == user_id

    def test_init_grades(self):
        assert user._grades == {"current_score": user_grade}

    def test_init_page_views(self):
        assert user._page_views == user_page_views

    def test_init_sortable_name(self):
        assert user.sortable_name == user_name

    def test_to_course_summary(self):
        assert user.to_course_summary() == {
            "id": user_id,
            "page_views": user_page_views,
        }


class TestMockCourse:
    min_date = datetime.datetime(2019, 1, 2, 10, 10, 1, 0, datetime.timezone.utc)
    max_date = datetime.datetime(2019, 1, 3, 10, 10, 1, 0, datetime.timezone.utc)
    user1 = MockUser(1, 100, "1", 1, min_date, max_date)
    user2 = MockUser(2, 100, "2", 2, min_date, max_date)
    course = MockCourse([user1, user2])

    def test_get_course_level_student_summary_data(self):
        assert self.course.get_course_level_student_summary_data() == [
            {"id": 1, "page_views": 1},
            {"id": 2, "page_views": 2},
        ]

    def test_get_user(self):
        with_id = self.course.get_user(1)
        with_user = self.course.get_user(self.user1)

        assert with_id is with_user
        assert with_id is self.user1

    def test_get_user_in_a_course_level_participation_data(self):
        with_id = self.course.get_user_in_a_course_level_participation_data(1)
        with_user = self.course.get_user_in_a_course_level_participation_data(
            self.user1
        )

        assert with_id == {
            "page_views": {"2019-01-02T10:10:01+0000": 1, "2019-01-03T10:10:01+0000": 1}
        }
        assert with_id == with_user


class TestMockCanvas:
    def test_course_404(self):
        course = MockCanvas.get_course(404)
        with pytest.raises(ResourceDoesNotExist):
            course.get_enrollments()
        with pytest.raises(ResourceDoesNotExist):
            course.get_course_level_student_summary_data()

    def test_course_0(self):
        course = MockCanvas.get_course(0)
        assert course.get_enrollments() == []
        assert course.get_course_level_student_summary_data() == []

    def test_404(self):
        with pytest.raises(ResourceDoesNotExist):
            MockCanvas.get_course(100)
