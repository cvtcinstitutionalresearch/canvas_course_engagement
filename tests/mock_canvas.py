"""Provides mock canvasapi objects for use in testing CCE with some pre-made instances"""

from unittest import mock
from canvasapi.exceptions import ResourceDoesNotExist
import datetime


class MockUser:
    """A mock of canvasapi.user.User with more data added for MockCourse's actions.

    :param user_id: An ID for this User

    :current_score: A percentage grade for this User

    :param name: This User's name

    :param page_views: The number of times this user has viewed their course

    :param first_view: The first time the user viewed their course

    :param last_view: The last time the user viewed their course
    """

    def __init__(
        self,
        user_id: int,
        current_score: float,
        name: str,
        page_views: int,
        first_view: datetime.datetime,
        last_view: datetime.datetime,
    ):
        self.id = user_id
        self._grades = {"current_score": current_score}
        self.sortable_name = name
        self._page_views = page_views
        self._first_view = first_view
        self._last_view = last_view

    def to_course_summary(self):
        """
        Returns the data required by student_engagement of
        MockCourse.get_course_level_student_summary_data
        """
        return {"id": self.id, "page_views": self._page_views}

    def to_view_summary(self):
        """
        Returns the data required for
        MockCourse.get_user_in_a_course_level_participation_data
        """
        return (self._first_view, self._last_view)

    def to_dict(self):
        return {"id": self.id, "sortable_name": self.sortable_name}


class MockEnrollment:
    def __init__(self, user: MockUser):
        self.grades = user._grades
        self._user = user
        self.user = user.to_dict()
        self.user_id = user.id

    def to_dict(self):
        return {"grades": self.grades, "user": self.user}


class MockCourse:
    """A mock of canvasapi.course.Course"""

    def __init__(self, users: (MockUser)):
        self.users = users

    def get_user(self, user, *args, **kwargs):
        try:
            user_id = user.id
        except AttributeError:
            user_id = user

        for user_test in self.users:
            if user_test.id == user_id:
                return user_test

        raise ResourceDoesNotExist("Did not find user")

    def get_course_level_student_summary_data(self, *args, **kwargs):
        return [user.to_course_summary() for user in self.users]

    def get_enrollments(self, *args, **kwargs):
        return [MockEnrollment(user) for user in self.users]

    def get_user_in_a_course_level_participation_data(self, user, *args, **kwargs):
        iso8601_datestring_notime = "%Y-%m-%dT%H:%M:%S%z"
        user = self.get_user(user)
        min_date = user._first_view
        max_date = user._last_view
        return {
            "page_views": {
                min_date.strftime(iso8601_datestring_notime): 1,
                max_date.strftime(iso8601_datestring_notime): 1,
            }
        }


user_unrated = MockUser(
    1,
    None,
    "user_unrated",
    0,
    datetime.datetime(2018, 10, 10),
    datetime.datetime(2018, 10, 11),
)
user_engage = MockUser(
    2,
    61.33,
    "user_engage",
    0,
    datetime.datetime(2018, 10, 12),
    datetime.datetime(2018, 10, 13),
)
user_explore = MockUser(
    3,
    71.67,
    "user_explore",
    1,
    datetime.datetime(2018, 10, 14),
    datetime.datetime(2018, 10, 15),
)
user_encourage = MockUser(
    4,
    100,
    "user_encourage",
    3,
    datetime.datetime(2018, 10, 16),
    datetime.datetime(2018, 10, 17),
)

course_1_users = [user_unrated, user_engage, user_explore, user_encourage]
# Course 2 is the same as course 1 but with a fake "Not found" page view count for one user
course_2_users = [
    user_unrated,
    user_engage,
    user_explore,
    user_encourage,
    MockUser(
        100, 90, "user_no_views", None, datetime.datetime.min, datetime.datetime.max
    ),
]


class MockCanvas:
    """A mock Canvas object for the API tests"""

    @classmethod
    def get_course(self, course_number):
        course = mock.MagicMock()
        if course_number == 1:
            course = MockCourse(course_1_users)
        elif course_number == 2:
            course = MockCourse(course_2_users)
        elif course_number == 404:
            course.get_enrollments = mock.Mock(
                side_effect=ResourceDoesNotExist("Mock lack of enrollments")
            )
            course.get_course_level_student_summary_data = mock.Mock(
                side_effect=ResourceDoesNotExist("Mock lack of student summaries")
            )
        elif course_number == 0:
            course = MockCourse([])
        else:
            raise ResourceDoesNotExist("Mock 404 error")

        return course
