canvas_course_engagement
========================

canvas_course_engagement (CCE) is a Python module which helps report on the engagement of students in a Canvas course. It uses the University of Michigan's `Student Explorer's algorithm <docs/_static/StudentRatingMap.png>`_ and may be run against any Canvas environment or course for which you have access.

Prerequisites
-------------

Using CCE requires Python 3.5 or later with pip.

Development
-----------

If you're a developer looking to use CCE in your own projects, you can install it with the following command::

    # pip
    pip install --user git+https://bitbucket.org/cvtcinstitutionalresearch/canvas_course_engagement/#egg=canvas_course_engagement

    # pipenv
    pipenv install git+https://bitbucket.org/cvtcinstitutionalresearch/canvas_course_engagement/#egg=canvas_course_engagement

With that done, the documentation can be found in the `docs <docs/>`_ folder of this repository.

Scripts
-------

This repository contains a collection of scripts which use `canvas_course_engagement <https://bitbucket.org/cvtcinstitutionalresearch/canvas_course_engagement/src>`_ to show information about a course or courses.

To use these scripts, you fill first need a Canvas API Key or Access Token. You can create one in the "Approved Integrations" section of your Canvas profile settings, ``https://your_canvas_server/profile/settings``.

For all of these scripts, if you would like to overwrite the output file no matter what, add the ``-o`` argument to your command. If you would like to not write headers to a new output file, add the ``-n`` argument.

Dump engagement information for one course
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You will need to know the course ID of the course you wish to dump, which is shown in the course URL (for example, https://your_canvas_server/courses/**course_id**). Once you have that, continue with the instructions below:

Linux
"""""

#. Clone CCE::

    git clone https://bitbucket.org/cvtcinstitutionalresearch/canvas_course_engagement/
    cd canvas_course_engagement

#. Install dependencies::

    pipenv install

#. Set required configuration::

    export CANVAS_API_URL='https://your_canvas_server'
    export CANVAS_API_KEY='Canvas API Key / Access Token'

#. Run the script::

    pipenv run python ./dump_course.py 'course ID' 'output file'

Windows (PowerShell)
""""""""""""""""""""

#. Clone CCE::

    git clone https://bitbucket.org/cvtcinstitutionalresearch/canvas_course_engagement/
    cd canvas_course_engagement

#. Install dependencies::

    pipenv install

#. Set required configuration::

    $env:CANVAS_API_URL='https://your_canvas_server'
    $env:CANVAS_API_KEY='Canvas API Key / Access Token'

#. Run the script::

    pipenv run python .\dump_course.py 'course ID' 'output file'

More information about using ``dump_course.py`` can be found at the top of the script.

Dump engagement information for all courses in an account
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Another example script included in this repository is ``dump_courses.py``. This script will dump information for all of the courses in an account in a term. To run this script, follow the instructions for "Dump engagement information for one course" above to clone and set the required configuration. Then, run the script as follows::

    pipenv run python ./dump_courses.py 'account ID' 'term ID' 'output file'

The term ID may be the numeric unique ID given to the term in Canvas (which can only be retrieved from the API) or the SIS ID which can be retrieved from ``/accounts/1/terms`` on your Canvas server.\

More information about using ``dump_courses.py`` and ``dump_courses_concise.py`` can be found at the top of their files.

Development API Server
----------------------

This repository contains an API server in `api.py <canvas_course_engagement/api.py>`_ which may be used to serve CCE data to other downstream projects. To run the development API server, use the following instructions on a Linux host with Python 3.5 or higher installed. Production deployment instructions can be found in `deploy-api.rst in the docs/ directory <docs/deploy-api.rst>`_

#. Install Pipenv, which manages the deployment dependencies::

    pip install pipenv

#. Clone this repository to your machine::

    git clone git+https://bitbucket.org/cvtcinstitutionalresearch/canvas_course_engagement/

#. Change into the new directory::

    cd canvas_course_engagement

#. Install the dependencies::

    pipenv install --deploy

#. Add the required configuration to the environment::

    export CANVAS_API_URL='https://your_canvas_server'

#. Run the server::

    pipenv run gunicorn canvas_course_engagement.api:app

By default, the server will listen on ``http://127.0.0.1:8000``. It accepts requests to ``/courses/:course_id`` and returns the API data about the course. It uses Bearer token authentication. The Bearer token may be an access token gained by `Canvas OAUTH2 <https://canvas.instructure.com/doc/api/file.oauth.html>`_ or your personal Settings page. For more information, see 'api.rst' in the docs folder.
