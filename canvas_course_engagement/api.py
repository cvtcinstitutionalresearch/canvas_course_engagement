from os import environ
import json

import falcon
from falcon import HTTPUnauthorized, HTTPNotFound
from canvasapi import Canvas
from canvasapi.exceptions import InvalidAccessToken, Unauthorized, ResourceDoesNotExist

from canvas_course_engagement.student_engagement import (
    StudentRating,
    get_ratings_for_course,
    rate,
)

try:
    API_URL = environ["CANVAS_API_URL"]
except KeyError:
    print("CANVAS_API_URL must be provided in the environment to use this script")
    exit(1)

app = falcon.API()


class CourseResource:
    def on_get(self, req, resp, course_id):
        resp.content_type = falcon.MEDIA_JSON

        if req.auth is not None and req.auth.lower().startswith("bearer "):
            canvas_token = req.auth[7:]
        else:
            raise HTTPUnauthorized(
                title="Auth token not provided",
                challenges=["Bearer", 'error="Auth token missing"'],
            )

        canvas = Canvas(API_URL, canvas_token)

        try:
            course = canvas.get_course(course_id)
            graded_course = get_ratings_for_course(course)
        except (InvalidAccessToken, Unauthorized):
            raise HTTPUnauthorized(
                title="Auth token incorrect",
                challenges=["Bearer", 'error="Invalid auth token"'],
            )
        except ResourceDoesNotExist:
            raise HTTPNotFound

        students = []
        for student_id, student in graded_course.students.items():
            students.append(
                {
                    "user_id": student_id,
                    "engagement": student["engagement"].value,
                    "grade": student["grade"],
                    "page_view_percentile": student["page_view_percentile"],
                    "page_views": student["page_views"],
                    "user": student["user"],
                }
            )
        resp.data = json.dumps(
            {
                "course_id": course.id,
                "average_grade": graded_course.average_grade,
                "course": {
                    "name": course.name,
                    "id": course.id
                },
                "students": students,
            }
        ).encode()


course_resource = CourseResource()
app.add_route("/courses/{course_id}", course_resource)
