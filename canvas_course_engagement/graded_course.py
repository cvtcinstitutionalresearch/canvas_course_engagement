class GradedCourse:
    """An object which contains information about all of the students in a course"""

    course = None
    """The ``canvasapi.Course.course`` object which this object describes"""

    average_grade = int()
    """The average of all students' grades in this course.
    Students with no grades do not factor into this average.
    """

    students = dict()
    """
    Contains a dict of students in this course::

        {
            user_id: {
                "engagement": engagement,
                "grade": grade,
                "page_view_percentile": page_view_percentile,
                "user": ``CanvasAPI.user.User``
            },
            [...],
        }
    """

    def __init__(self, course, average_grade, students):
        self.course = course
        self.average_grade = average_grade
        self.students = students
