from enum import Enum
from statistics import mean

import scipy.stats
import canvasapi.exceptions
from datetime import datetime

from .graded_course import GradedCourse


class StudentRating(Enum):
    """ A three-value rating of a student's performance

    * ENCOURAGE is a rating given to "encourage student to continue"
    * EXPLORE is a rating given to "explore options to improve"
    * ENGAGE is a rating given to "engage the student in the course, now"
    * UNRATED indicates that not enough data was available to rate the student
    """

    ENCOURAGE = "Encourage"
    EXPLORE = "Explore"
    ENGAGE = "Engage"
    UNRATED = "Unrated"


def rate(student_grade, course_average_grade, student_page_views_percentile):
    """ Rates a student based on their grade and how active they are in their course

    .. image:: /_static/StudentRatingMap.png
       :alt: Map showing student rating logic

    :param student_grade: The student's overall grade as a percentage (eg. 67.7)

    :param course_average_grade: The average grade for the entire course as a percentage
                                 (eg. 67.7)

    :param student_page_views_percentile:
        The student's percentile rank of course page views. This can be obtained by
        ranking an entire course's results from, for example,
        `GET /api/v1/courses/:course_id/analytics/users/:student_id/activity
        <https://canvas.instructure.com/doc/api/analytics.html#method.analytics_api.student_in_course_participation>`_

    :rtype: :class:`StudentRating`
    """

    if student_grade is None:
        return StudentRating.UNRATED

    if student_grade >= 85:
        return StudentRating.ENCOURAGE
    elif student_grade < 55:
        return StudentRating.ENGAGE

    if 55 <= student_grade < 65:
        if (course_average_grade - student_grade) <= 10:
            return StudentRating.EXPLORE
        return StudentRating.ENGAGE

    course_average_grade_difference = student_grade - course_average_grade

    if student_grade >= 75:
        if (
            course_average_grade_difference >= -15
            or student_page_views_percentile >= 25
        ):
            return StudentRating.ENCOURAGE
        return StudentRating.EXPLORE
    else:
        if (
            course_average_grade_difference >= -15
            or student_page_views_percentile >= 25
        ):
            return StudentRating.EXPLORE
        return StudentRating.ENGAGE


def get_current_student_scores(course):
    """Retrieves the current score of all students in a course

    :param course: Course to retrieve average scores for
    :type course: ``canvasapi.course.Course``

    :returns:
        ``dict`` in the format::

            {
                student_id: {
                    "score": score,
                    "user": ``dict`` from enrollments, see https://canvas.instructure.com/doc/api/enrollments.html
                }
            }

    .. note::

        If there are no grades posted for a user, "score" will be ``None``.
    """

    try:
        enrollments = course.get_enrollments(
            type=["StudentEnrollment"],
            state=["active", "completed", "invited"],
            per_page=100,
        )
    except AttributeError:
        raise TypeError("course is not a canvasapi.course.Course")

    current_student_scores = {}
    for enrollment in enrollments:
        current_student_scores[enrollment.user_id] = {
            "score": enrollment.grades["current_score"],
            "user": enrollment.user,
        }

    return current_student_scores


def get_student_access_for_course(course):
    """ Returns the number of times a student accessed any page in a course

    :param course: Course to get student access for
    :type course: ``canvasapi.course.Course``

    :returns:
        ``dict`` in the format::

            {user_id: page_views}

        Where user_id is the id of the student in Canvas.

    .. warning::

        If a student has not viewed a course at all, they may not be contained in the
        returned ``dict``. Be aware of this and assume zero if your desired student is
        not found in the ``dict``.
    """
    course_student_page_views = {}

    # If there are no analytics, the Canvas API may return a 404
    try:
        student_summaries = course.get_course_level_student_summary_data(per_page=100)
    except AttributeError:
        raise TypeError("course is not a canvasapi.course.Course")
    except canvasapi.exceptions.ResourceDoesNotExist:
        return {}

    for student_summary in student_summaries:
        this_student_id = student_summary["id"]
        this_student_page_view_count = student_summary["page_views"]
        course_student_page_views[this_student_id] = this_student_page_view_count
    return course_student_page_views


def get_ratings_for_course(course):
    """ Returns the student engagement rating for every student in a course.

    :param course: Course to get student engagement for
    :type course: ``canvasapi.course.Course``

    :returns:
        :class:`canvas_course_engagement.graded_course.GradedCourse`.
    """

    student_score_info = get_current_student_scores(course)
    student_scores = {}

    for student_id, student in student_score_info.items():
        score = student["score"]

        # Any student with no grades will have None as their grade
        if score is None:
            continue

        student_scores[student_id] = score

    if len(student_scores) == 0:
        return GradedCourse(course, None, {})

    average_score = mean(student_scores.values())
    student_page_views = get_student_access_for_course(course)

    page_views_list = [
        count for count in student_page_views.values() if count is not None
    ]

    student_access_percentiles = {}
    for student_id in student_score_info.keys():
        try:
            page_views = student_page_views[student_id]
        except KeyError:
            page_views = None

        if page_views is not None:
            site_views_percentile_rank = scipy.stats.percentileofscore(
                page_views_list, page_views
            )
        else:
            site_views_percentile_rank = 0

        student_access_percentiles[student_id] = site_views_percentile_rank

    student_ratings = {}
    for student_id, student in student_score_info.items():
        student_id = student["user"]["id"]
        student_grade = student["score"]
        student_user = student["user"]
        student_access_percentile = student_access_percentiles[student_id]
        try:
            student_page_view = student_page_views[student_id]
        except KeyError:
            student_page_view = 0
        student_rating = rate(student_grade, average_score, student_access_percentile)

        student_ratings[student_id] = {
            "engagement": student_rating,
            "grade": student_grade,
            "page_view_percentile": student_access_percentile,
            "page_views": student_page_view,
            "user": student_user,
        }

    graded_course = GradedCourse(course, average_score, student_ratings)

    return graded_course


def try_to_parse_rfc3339(string):
    """
    Attempts to input a string using an almost RFC 3339 format (the closest we can get
    before Python 3.7). If that fails, removes the last colon and tries again.

    :returns: datetime.datetime object with the specified time.
    """
    rfc3339_localtimestring = "%Y-%m-%dT%H:%M:%S%z"

    try:
        return datetime.strptime(string, rfc3339_localtimestring)
    except ValueError:
        li = string.rsplit(":", 1)
        fixed = "".join(li)
        return datetime.strptime(fixed, rfc3339_localtimestring)


def get_first_and_last_view_for_student(course, student):
    """Returns the first and last time the student viewed the course

    course should be a canvasapi.course.Course

    student should be a Canvas User ID or canvasapi.user.User

    :returns:
        Tuple of the format (datetime.datetime, datetime.datetime) where the first item
        is the first view and the second is the last view
    """

    try:
        view_data = course.get_user_in_a_course_level_participation_data(student)
    except canvasapi.exceptions.ResourceDoesNotExist:
        return (None, None)

    page_views = view_data["page_views"]
    view_times = page_views.keys()

    if view_times:
        first_view = try_to_parse_rfc3339(min(view_times))
        last_view = try_to_parse_rfc3339(max(view_times))
        return (first_view, last_view)

    return (None, None)
