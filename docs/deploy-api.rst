Deploy API server
=================

These instructions will lead you through deploying the API server on a RHEL7 server. We will stop short of setting up a reverse proxy to access the API through.

You will need:

* RHEL7 server
* `RedHat Software Collections enabled and python3.6 installed <https://developers.redhat.com/blog/2018/08/13/install-python3-rhel/>`_

Prepare environment
-------------------

We will run the CCE API under its own user account. Create it with the following command::

    sudo adduser --system --create-home --user-group cce

Install Pipenv
--------------

Now we can install Pipenv for the new user. Run the following command to set it up::

    sudo -Hu cce scl enable rh-python36 -- pip3 install pipenv --user

This uses the RedHat-supported Python3 from Software Collections to install pipenv.

Clone source
------------

Before we can run the CCE API, we need its source cloned. To ensure that the server will continue to be able to clone the source and have an up-to-date copy, we'll give it an SSH access key.

First, let's get a shell for the cce user::

    sudo -iu cce

Now we can clone the source. Issue the following command to do it now::

    git clone https://bitbucket.org/cvtcinstitutionalresearch/canvas_course_engagement.git /home/cce/canvas_course_engagement

With that, the ``canvas_course_engagement`` source can be found at ``/home/cce/canvas_course_engagement``.

Install dependencies
--------------------

Using ``pipenv`` we can set up the environment for the CCE API and install the dependencies::

    cd ~/canvas_course_engagement
    scl enable rh-python36 bash
    pipenv install --python=3.6 --deploy

Run server
----------

To test the server, you can run it with the following command::

    CANVAS_API_URL='https://institution.instructure.com' pipenv run gunicorn canvas_course_engagement.api:app

The API will be served on port 8000.

Manage with systemd
-------------------

To run the app with systemd, use the following configuration. Place this file in ``/etc/systemd/system/cce_api.service`` and modify any configuration you need. You will definitely need to modify the ``CANVAS_API_URL`` variable.

.. literalinclude:: /_static/cce-api.service
   :caption: /etc/systemd/system/cce-api.service

When you're ready to enable and run it, run the following commands::

    sudo systemctl daemon-reload
    sudo systemctl enable --now cce-api.service

The API will now be running on port 7381 and will start with the machine. It will only be accessible on the local machine. If you would like to access the API from a different computer, replace ``localhost`` in ``localhost:7381`` in the service file with the IP you would like the server to listen on.
