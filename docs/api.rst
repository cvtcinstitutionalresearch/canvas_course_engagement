REST API
========

CanvasCourseEngagement includes a RESTful API which can be used to query information about student engagement in a course.

Courses
-------

.. http:get:: /courses/(int:course_id)

    Retrieve student engagement information for `course_id`.

    This endpoint uses Bearer Token authentication. If a token is not provided or incorrect, the ``WWW-Authenticate`` header will be returned with the type of authenticated needed and an ``error:`` field.

    **Example request**:

    .. sourcecode:: http

        GET /courses/1 HTTP/1.1
        Host: school.instructure.com
        Accept: application/json
        Authorization: Bearer MY_AWESOME_TOKEN

    **Response format**

    .. code:: js

        {
            "course_id": Number,
            "average_grade": Number,
            "course": {
                "name": String, name of the course,
                "id": Number, the course ID number
            }
            "students": [
                {
                    "user_id": Number,
                    "engagement": "",
                    "grade": Number or null,
                    "page_view_percentile": Number or null,
                    "page_views": Number or null,
                    "user": {
                        // User object as found on an Enrollment object
                        // see https://canvas.instructure.com/doc/api/enrollments.html
                    }
                }
            ]
        }

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "course_id": 1,
            "average_grade": 70.7,
            "course": {
                "id": 525,
                "name": "Test Course",
                "account_id": 85,
                "uuid": "MgyHWyCruiW4LUfTa3HRVKg44YkeJNGQmWJxhRMn",
                "start_at": "2018-10-25T15:49:21Z",
                "grading_standard_id": null,
                "is_public": null,
                "created_at": "2018-10-25T14:48:27Z",
                "course_code": "Test Course",
                "default_view": "feed",
                "root_account_id": 1,
                "enrollment_term_id": 1,
                "end_at": null,
                "public_syllabus": false,
                "public_syllabus_to_auth": false,
                "storage_quota_mb": 1000,
                "is_public_to_auth_users": false,
                "hide_final_grades": false,
                "apply_assignment_group_weights": false,
                "calendar": {
                    "ics": "https://institution.instructure.com/feeds/calendars/course_MgyHWyCruiW4LUfTa3HRVKg44YkeJNGQmWJxhRMn.ics"
                },
                "time_zone": "America/Chicago",
                "blueprint": false,
                "sis_course_id": null,
                "sis_import_id": null,
                "integration_id": null,
                "enrollments": [
                    {
                        "type": "teacher",
                        "role": "TeacherEnrollment",
                        "role_id": 4,
                        "user_id": 993,
                        "enrollment_state": "active"
                    }
                ],
                "workflow_state": "available",
                "restrict_enrollments_to_course_dates": false
            }
            "students": [
                {
                    "user_id": 4567,
                    "engagement": "Engage",
                    "grade": 61.4,
                    "page_view_percentile": 0
                    "page_views": 0
                    "user": {
                        "id": 4567,
                        "name": "Testing Student 1",
                        "sortable_name": "1, Testing Student",
                        "short_name": "Testing Student 1"
                    }
                },
                {
                    "user_id": 3712,
                    "engagement": "Encourage",
                    "grade": 80,
                    "page_view_percentile": 100
                    "page_views": 1
                    "user": {
                        "id": 3712,
                        "name": "Testing Student 2",
                        "created_at": "2018-10-25T09:54:03-05:00",
                        "sortable_name": "2, Testing Student",
                        "short_name": "Testing Student 2",
                        "sis_user_id": null,
                        "integration_id": null,
                        "sis_import_id": null,
                        "login_id": "teststudent2"
                    }
                }
            ]
        }
