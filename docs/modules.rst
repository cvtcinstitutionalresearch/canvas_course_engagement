canvas_course_engagement
========================

This page contains the documentation for canvas_course_engagement's library API.

canvas\_course\_engagement.student\_engagement
----------------------------------------------

The student_engagement module contains all of the logic required to get information about student engagement for a single Canvas course. See :func:`canvas_course_engagement.student_engagement.get_ratings_for_course` for the function to do that.

.. automodule:: canvas_course_engagement.student_engagement
    :members:
    :undoc-members:
    :show-inheritance:

canvas\_course\_engagement.graded\_course
-----------------------------------------

.. automodule:: canvas_course_engagement.graded_course
    :members:
    :undoc-members:
    :show-inheritance:
