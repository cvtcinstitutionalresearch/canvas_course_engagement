from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, "README.rst"), encoding="utf-8") as f:
    long_description = f.read()

setup(
    name="canvas_course_engagement",
    version="1.0.1",
    url="https://bitbucket.org/cvtcinstitutionalresearch/canvas_course_engagement",
    author="Dalton Durst",
    author_email="ddurst2@cvtc.edu",
    description="Tools to determine the engagement of students in a Canvas course",
    packages=find_packages(exclude=["contrib", "docs", "tests", "run"]),
    long_description=long_description,
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Education",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7"
    ],
    install_requires=["scipy", "canvasapi"],
    extras_require={"dev": ["sphinx", "sphinx-rtd-theme", "pytest"],
                    "api": ["falcon", "gunicorn", "gevent"]},
)
