"""
A script which uses canvas_course_engagement to dump information about a course into
a .csv file

To use this script:
    Set CANVAS_API_URL in the environment to the URL of your canvas server (like https://institution.instructure.com)
    Set CANVAS_API_KEY in the environment to your access token (get one from https://institution.instructure.com/profile/settings)
    Run as "python dump_course.py {Course ID number} {Output file}"
        {Course ID number} can be gained from the URL of your course in Canvas
        {Output file} will be created if it doesn't exist or overwritten if it does
"""

from os import environ
import argparse
import csv
from datetime import datetime

from canvasapi import Canvas

from canvas_course_engagement.student_engagement import (
    StudentRating,
    get_ratings_for_course,
    rate,
)

try:
    API_URL = environ["CANVAS_API_URL"]
except KeyError:
    print("CANVAS_API_URL not found in the environment")
    exit(1)

try:
    API_KEY = environ["CANVAS_API_KEY"]
except KeyError:
    print("CANVAS_API_KEY not found in the environment")
    exit(1)

parser = argparse.ArgumentParser(
    description="Dump student engagement information about a course into a .csv file"
)
parser.add_argument("course", type=int, help="Course to dump information from")
parser.add_argument("file", type=str, help="File to dump information into")
parser.add_argument(
    "-o|--overwrite",
    dest="overwrite",
    action="store_true",
    help="Overwrite the file, if it exists",
)
parser.add_argument(
    "-n|--no-headers",
    dest="no_headers",
    action="store_true",
    help="Do not add headers to the CSV file if it does not exist",
)

args = parser.parse_args()

outfile = args.file
course_id = args.course
overwrite_outfile = args.overwrite
no_headers = args.no_headers

canvas = Canvas(API_URL, API_KEY)
course = canvas.get_course(course_id)

course_engagement = get_ratings_for_course(course)

# Decide whether the file needs headers
if not overwrite_outfile:
    try:
        with open(outfile, "r"):
            needs_headers = False
    except OSError:
        needs_headers = True
elif no_headers:
    needs_headers = False
else:
    needs_headers = True

mode = "w" if overwrite_outfile else "a"

with open(outfile, mode, newline="") as csvfile:
    engagement_writer = csv.writer(csvfile)
    if needs_headers:
        engagement_writer.writerow(
            [
                "Date",
                "CourseID",
                "StudentID",
                "SortableName",
                "Engagement Number",
                "Engagement Value",
                "Grade",
                "PageViewPercentile",
            ]
        )

    for student_id, student in course_engagement.students.items():
        engagement_value = student["engagement"].value
        engagement_num = -1

        # Sorted weirdly for statistical short-circuiting
        if engagement_value == "Encourage":
            engagement_num = 3
        elif engagement_value == "Engage":
            engagement_num = 1
        elif engagement_value == "Explore":
            engagement_num = 2
        elif engagement_value == "Unrated":
            engagement_num = 0

        engagement_writer.writerow(
            [
                datetime.now().strftime("%Y/%m/%d"),
                course.sis_course_id,
                student["user"]["id"],
                student["user"]["sortable_name"],
                engagement_num,
                engagement_value,
                student["grade"],
                student["page_view_percentile"],
            ]
        )
