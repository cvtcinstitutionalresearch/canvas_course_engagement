"""
A script which uses canvas_course_engagement to dump information about all Canvas
courses of an account into a .csv file

To use this script:
    Set CANVAS_API_URL in the environment to the URL of your canvas server
        (like https://institution.instructure.com)
    Set CANVAS_API_KEY in the environment to your access token
        (get one from https://institution.instructure.com/profile/settings)
    Run as "python dump_courses.py [-o] [-n] {Course ID number} {Term} {Output file}"
        For more information, see the Canvas Course Engagement README:
        https://bitbucket.org/cvtcinstitutionalresearch/canvas_course_engagement

CSV format:

Date (Date script was started)
Key
SISCourseID
CourseID
CourseStartDate
CourseEndDate
Instructor
TermID
SISTermID
StudentID
SortableName
Engagement
Grade
PageViewPercentile
PageViews
FirstView
LastView
"""

from os import environ
import argparse
import csv
from datetime import datetime
import traceback
import time

from canvasapi import Canvas
from canvasapi.exceptions import InvalidAccessToken, ResourceDoesNotExist

from canvas_course_engagement.student_engagement import (
    StudentRating,
    get_ratings_for_course,
    get_first_and_last_view_for_student,
    rate,
)


class RetryError(Exception):
    """Thrown when the method called by retry() fails on its last try"""


def retry(method, tries, delay, *args, **kwargs):
    """ Retries a function call on any exception up to ``tries`` times

    :param method: The method to call

    :param tries: Number of times to retry the call

    :param delay: Number of seconds between retries

    :args: Arguments to pass to method

    :kwargs: Keyword arguments to pass to method
    """

    failed = False

    for i in range(tries):
        failed = False
        try:
            return method(*args, **kwargs)
        except Exception:
            traceback.print_exc()
            print("Caught exception on try", i + 1, "of", tries)
            failed = True
        time.sleep(delay)

    if failed:
        raise RetryError(method, *args, **kwargs)


parser = argparse.ArgumentParser(
    description="Dump student engagement information about a course into a .csv file"
)
parser.add_argument("account", type=int, help="Account to dump course information from")
parser.add_argument(
    "term", type=str, help="Enrollment term to get course information for"
)
parser.add_argument("file", type=str, help="File to dump information into")
parser.add_argument(
    "-o|--overwrite",
    dest="overwrite",
    action="store_true",
    help="Overwrite the file, if it exists",
)
parser.add_argument(
    "-n|--no-headers",
    dest="no_headers",
    action="store_true",
    help="Do not add headers to the CSV file if it does not exist",
)

args = parser.parse_args()

try:
    API_URL = environ["CANVAS_API_URL"]
except KeyError:
    print("CANVAS_API_URL not found in the environment")
    exit(1)

try:
    API_KEY = environ["CANVAS_API_KEY"]
except KeyError:
    print("CANVAS_API_KEY not found in the environment")
    exit(1)

outfile = args.file
account_id = args.account
term_id = args.term
overwrite_outfile = args.overwrite
no_headers = args.no_headers

canvas = Canvas(API_URL, API_KEY)

try:
    root_account = canvas.get_account(1)
except InvalidAccessToken:
    print("CANVAS_API_KEY is invalid for " + API_URL)
    exit(3)

account = canvas.get_account(account_id)
terms = root_account.get_enrollment_terms()
selected_term = None
for term in terms:
    try:
        int_term_id = int(term_id)
    except ValueError:
        int_term_id = None
    if term.sis_term_id == term_id:
        selected_term = term
    elif term.id == int_term_id:
        selected_term = term

if not selected_term:
    print(
        'No term found in "{}" with the ID or SIS ID "{}".'.format(
            account.name, term_id
        )
    )
    exit(1)

print(
    'Selected term is "{}", SIS ID "{}"'.format(
        selected_term.name, selected_term.sis_term_id
    )
)

print("Finding courses...")
time.sleep(5)
courses = [
    course
    for course in retry(
        account.get_courses, 3, 10, per_page=1000, enrollment_term_id=selected_term.id
    )
]
total_courses = len(courses)
print("We have {} courses to get information for...".format(total_courses))

# Decide whether the file needs headers
if not overwrite_outfile:
    try:
        with open(outfile, "r"):
            print("File exists, we don't need to write headers.")
            needs_headers = False
    except OSError:
        print("File does not exist, we need to write headers.")
        needs_headers = True
elif no_headers:
    print("User requested we don't write headers.")
    needs_headers = False
else:
    print("Writing headers")
    needs_headers = True

mode = "w" if overwrite_outfile else "a"

with open(outfile, mode, newline="") as csvfile:
    engagement_writer = csv.writer(csvfile)
    if needs_headers:
        engagement_writer.writerow(
            [
                "Date",
                "Key",
                "SISCourseID",
                "CourseID",
                "CourseStartDate",
                "CourseEndDate",
                "Instructor",
                "TermID",
                "SISTermID",
                "StudentID",
                "SortableName",
                "Engagement",
                "Grade",
                "PageViewPercentile",
                "PageViews",
                "FirstView",
                "LastView",
            ]
        )

    courses_finished = 0
    out_datestring = "%Y/%m/%d"
    almost_rfc3339_timestring = "%Y-%m-%dT%H:%M:%SZ"
    almost_rfc3339_localtimestring = "%Y-%m-%dT%H:%M:%S%z"
    retrieved_date_str = datetime.utcnow().strftime(out_datestring)
    for course in courses:
        course_engagement = retry(get_ratings_for_course, 5, 1, course)
        try:
            course_start_date = datetime.strptime(
                course.start_at, almost_rfc3339_timestring
            )
        except TypeError:
            course_start_date = datetime.min

        instructors = []
        instructor_enrollments = retry(
            course.get_enrollments,
            5,
            1,
            type=["TeacherEnrollment"],
            state=["active", "completed", "invited"],
            per_page=100,
        )
        for instructor_enrollment in instructor_enrollments:
            instructors.append(instructor_enrollment.user["sortable_name"])

        instructor_str = "; ".join(instructors)

        try:
            course_end_date = datetime.strptime(
                course.end_at, almost_rfc3339_timestring
            )
        except TypeError:
            course_end_date = datetime.max

        for student_id, student in course_engagement.students.items():
            (first_view, last_view) = retry(
                get_first_and_last_view_for_student, 5, 1, course, student_id
            )

            engagement_writer.writerow(
                [
                    retrieved_date_str,
                    "_".join(
                        [retrieved_date_str, str(course.id), str(student["user"]["id"])]
                    ),
                    course.sis_course_id,
                    course.id,
                    course_start_date.strftime(out_datestring),
                    course_end_date.strftime(out_datestring),
                    instructor_str,
                    selected_term.id,
                    selected_term.sis_term_id,
                    student["user"]["sis_user_id"],
                    student["user"]["sortable_name"],
                    student["engagement"].value,
                    student["grade"],
                    student["page_view_percentile"],
                    student["page_views"],
                    first_view.strftime(out_datestring) if first_view else "",
                    last_view.strftime(out_datestring) if last_view else "",
                ]
            )
        courses_finished += 1
        print(
            '[{done}/{total}] Got information for "{course}".'.format(
                done=courses_finished, total=total_courses, course=course.name
            )
        )
